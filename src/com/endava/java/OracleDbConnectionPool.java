package com.endava.java;
import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.SQLException;

public class OracleDbConnectionPool extends ObjectPoolOld<Connection> {

  private String address, user, password;

  public OracleDbConnectionPool(String driver, String address, String user, String password) {
    super();
    try {
      Class.forName(driver).newInstance();
    } catch (Exception e) {
      e.printStackTrace();
    }
    this.address = address;
    this.user = user;
    this.password = password;
  }


  @Override
  public boolean isValid(Connection o) {
    try {
      return (!o.isClosed());
    } catch (SQLException e) {
      e.printStackTrace();
      return false;
    }
  }

@Override
public Connection createInstance() {
	  try {
	      return (DriverManager.getConnection(address, user, password));
	    } catch (SQLException e) {
	      e.printStackTrace();
	      return null;
	    }
}



@Override
public void closeInstance(Connection o) {
	String x[];
	
	x = new String[4];
	
	System.out.println(x[0]);

	try {
	      ((Connection) o).close();
	    } catch (SQLException e) {
	      e.printStackTrace();
	    }
	
}
}


