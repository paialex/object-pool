package com.endava.java;

import java.util.Iterator;
import java.util.LinkedHashMap;
import java.util.Map;
import java.util.Map.Entry;
import org.slf4j.*;

public abstract class ObjectPoolOld<T> {
	
	static Logger logger = LoggerFactory.getLogger(ObjectPool.class);

	public static final int DEFAULT_POOLSIZE = 3;
	public static final long DEFAULT_KILLTIME = 5000;
	
	private int poolSize;
	private long killTime;
	private final Map<T, Long> avilable;
	private final Map<T, Long> unavilable;
	private boolean avilableSpace;

	public ObjectPoolOld() {
		setPoolSize(4);
		setKillTime(4000);
		avilable = new LinkedHashMap<>();
		unavilable = new LinkedHashMap<>();
	}

	// Setters & Getters
	
	public int getPoolSize() {
		return poolSize;
	}

	public void setPoolSize(int poolSize) {
		if (poolSize > 0) {
			this.poolSize = poolSize;
		} else {
			poolSize = DEFAULT_POOLSIZE;
		}
	}

	public long getKillTime() {
		return killTime;
	}

	public void setKillTime(long killTime) {
		if (killTime > 0) {
			this.killTime = killTime;
		} else {
			this.killTime = DEFAULT_KILLTIME;
		}
	}

	// Abstract methods
	
	public abstract T createInstance();

	public abstract void closeInstance(T t);

	public abstract boolean isValid(T t);

	// Private methods
	
	private T validateOrCreateInstance(T instance, long currentTime) {
		if (isValid(instance)) {

			avilable.remove(instance);
			return switchToUnavilable(instance, currentTime);
		} else {
			avilable.remove(instance);
			closeInstance(instance);
			T t1 = createInstance();
			instance = t1;
			return switchToUnavilable(instance, currentTime);
		}
	}

	private T waitOrException(Map<T, Long> avilableInstances, long waitTime, T instance, long currentTime)
			throws NoMoreConnectionsException {
		avilableSpace = false;
		while (!avilableSpace) {
			try {
				wait(waitTime);
			} catch (InterruptedException e) {
				e.printStackTrace();
			}
		}
		if (avilableInstances.size() > 0) {
			instance = avilableInstances.keySet().iterator().next();
			return switchToUnavilable(instance, currentTime);
		} else {
			throw new NoMoreConnectionsException("No more connections avilable!");
		}
	}

	private T switchToUnavilable(T instance, long currentTime) {
		unavilable.put(instance, currentTime);
		return instance;
	}
	
	@SuppressWarnings("unchecked")
	private T checkAliveInstances(long now, Map<T, Long> map){
		Iterator<Entry<T, Long>> iterator = map.entrySet().iterator();
		T toReturn = null;
		while (iterator.hasNext()) {
			Entry<T, Long> entry = iterator.next();
			if (((now - entry.getValue()) > killTime) || (entry.getKey() == null)) {
				closeInstance((T) iterator.next());
				iterator.remove();
			} else {
				toReturn = entry.getKey();
				break;
			}
		}
		return toReturn;
	}

	// Public methods
	
	public synchronized T takeInstance() throws NoMoreConnectionsException {
		return takeInstance(0);
	}
	
	public synchronized T takeInstance(long waitTime) throws NoMoreConnectionsException {
		T instance = null;
		long currentTime = System.currentTimeMillis();

		if (avilable.size() > 0) {
			instance = checkAliveInstances(currentTime, avilable);
			if (instance != null) {
				return validateOrCreateInstance(instance, currentTime);
			}else{
				instance = createInstance();
				return switchToUnavilable(instance, currentTime);
			}
		}
		
		if ((unavilable.size() < poolSize) && avilable.isEmpty()) {
			instance = createInstance();
			return switchToUnavilable(instance, currentTime);
		} else {
			return waitOrException(avilable, waitTime, instance, currentTime);
		}

	}

	public synchronized void returnInstance(T instance) {
		if (unavilable.remove(instance) != null) {
			avilable.put(instance, System.currentTimeMillis());
			avilableSpace = true;
			notify();
			logger.info("CONNECTION WAS RETURNED BY LOGGER");
		}
	}
}
