package com.endava.java;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

public class SomeConnection {
	final static Logger logger = LoggerFactory.getLogger(SomeConnection.class); 
	public static int count = 0;
	

	public SomeConnection(){
		count++;
		logger.info("Connection # " + count + " was created");
	}

	
}
