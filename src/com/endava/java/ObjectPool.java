package com.endava.java;

import java.util.Iterator;
import java.util.LinkedHashMap;
import java.util.Map;
import java.util.Map.Entry;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;





public abstract class ObjectPool<T> {

	static Logger logger = LoggerFactory.getLogger(ObjectPool.class);
	
	public static final int DEFAULT_POOLSIZE = 3;
	public static final long DEFAULT_KILLTIME = 5000;

	private int poolSize;
	private long killTime;
	private final Map<T, Long> avilable;
	private final Map<T, Long> unavilable;
	private boolean avilableSpace;

	public ObjectPool() {
		setPoolSize(4);
		setKillTime(4000);
		avilable = new LinkedHashMap<>();
		unavilable = new LinkedHashMap<>();
	}

	// Setters & Getters

	public int getPoolSize() {
		return poolSize;
	}

	public void setPoolSize(int poolSize) {
		if (poolSize > 0) {
			this.poolSize = poolSize;
		} else {
			poolSize = DEFAULT_POOLSIZE;
		}
	}

	public long getKillTime() {
		return killTime;
	}

	public void setKillTime(long killTime) {
		if (killTime > 0) {
			this.killTime = killTime;
		} else {
			this.killTime = DEFAULT_KILLTIME;
		}
	}

	// Abstract methods

	public abstract T createInstance();

	public abstract void closeInstance(T t);

	public abstract boolean isValid(T t);

	// Private methods

	private T getValidInstance(long currentTime) {

		T instance = getAliveInstances(currentTime, avilable);

		avilable.remove(instance);
		if ( (instance == null) || !isValid(instance)){
			closeInstance(instance);
			instance = createInstance();
		}
		return switchToUnavilable(instance, currentTime);

	}

	private T getInstanceFromPool(long currentTime) {
		T instance = null;
		if (avilable.size() > 0) {
			instance = getValidInstance(currentTime);
		}

		else {
			instance = switchToUnavilable(createInstance(), currentTime);
		}
		return instance;
	}

	private T waitAnInstance(Map<T, Long> avilableInstances, long waitTime, long currentTime)
			throws NoMoreConnectionsException {
		T instance = null;
		avilableSpace = false;
		
		logger.info(Thread.currentThread().getName() + " is waiting.");
		while (!avilableSpace) {
			try {
				wait(waitTime);
			} catch (InterruptedException e) {
				e.printStackTrace();
			}
		}
		if (avilableInstances.size() > 0) {
			instance = switchToUnavilable(getValidInstance(currentTime), currentTime);
			logger.info(Thread.currentThread().getName() + " was taken an instance.");
		} else {
			logger.info("No more connections for " + Thread.currentThread().getName());
			throw new NoMoreConnectionsException("No more connections avilable!");
		}
		return instance;
	}

	private T switchToUnavilable(T instance, long currentTime) {
		unavilable.put(instance, currentTime);
		return instance;
	}

	@SuppressWarnings("unchecked")
	private T getAliveInstances(long now, Map<T, Long> map) {
		T toReturn = null;
		Iterator<Entry<T, Long>> iterator = map.entrySet().iterator();

		while (iterator.hasNext()) {
			Entry<T, Long> entry = iterator.next();
			if ((entry.getKey() == null) && ((now - entry.getValue()) > killTime)) {
				closeInstance((T) iterator.next());	
				logger.info("Insatnce " + iterator.next() + " is died");			
				iterator.remove();		
			} else {
				toReturn = entry.getKey();
				break;
			}
		}
		return toReturn;
	}

	// Public methods

	public synchronized T getInstance() throws NoMoreConnectionsException {
		return getInstance(0);
	}

	public synchronized T getInstance(long waitTime) throws NoMoreConnectionsException {
		T instance = null;
		long currentTime = System.currentTimeMillis();

		if (unavilable.size() < poolSize) {
			instance = getInstanceFromPool(currentTime);
		}

		else {
			instance = waitAnInstance(avilable, waitTime, currentTime);
		}
		logger.info("Instance was taken by " + Thread.currentThread().getName());
		return instance;

	}

	public synchronized void returnInstance(T instance) {
		if (unavilable.remove(instance) != null) {
			avilable.put(instance, System.currentTimeMillis());
			avilableSpace = true;
			notify();
			logger.info("Instance was returned by " + Thread.currentThread().getName());
		}
	}
}
