package com.endava.java;
import java.sql.Connection;
import java.sql.ResultSet;
import java.sql.ResultSetMetaData;
import java.sql.SQLException;
import java.sql.Statement;





public class Main {


  public static void main(String args[]) throws NoMoreConnectionsException, SQLException {
    
	  
	  
	
  
///////////////////////////////////////////////// Database Connection Test //////////////////////////////////////////////////////////////////	  
	  

/*
    // Create the ConnectionPool:
    ObjectPoolOld<Connection> pool1 = new OracleDbConnectionPool(
    		"oracle.jdbc.driver.OracleDriver", "jdbc:oracle:thin:@localhost:1521:xe",
    		"HR", "HR");

	  
    // Get a connection:
    Connection con = null;
    Connection con1 = null;
  
  
	con = pool1.createInstance();
	con1 = pool1.createInstance();
	
	

    
	    String query = "SELECT LAST_NAME, FIRST_NAME FROM EMPLOYEES WHERE SALARY > 10000";		
		
		Statement stm = null;
		ResultSet rs = null;
		ResultSetMetaData rsmd = null;
		
		try {
			stm = con.createStatement();
			rs = stm.executeQuery(query);
			rsmd = rs.getMetaData();
			System.out.format("%15s | %15s |\n",rsmd.getColumnName(1),rsmd.getColumnName(2));
			System.out.println("-----------------------------------");
			while (rs.next()) {
				System.out.format("%15s | %15s |\n",rs.getString(1), rs.getString("FIRST_NAME"));			
			}
			System.out.println("-----------------------------------");
			
		} catch (SQLException e) {
			
			e.printStackTrace();
		}finally {
			rs.close();
			stm.close();
			con.close();	
		}

 
		System.out.println();
		
		
		
		
String query1 = "SELECT LAST_NAME, FIRST_NAME FROM EMPLOYEES WHERE SALARY > 15000";		
		
		Statement stm1 = null;
		ResultSet rs1 = null;
		ResultSetMetaData rsmd1 = null;
		
		try {
			stm1 = con1.createStatement();
			rs1 = stm1.executeQuery(query1);
			rsmd1 = rs1.getMetaData();
			System.out.format("%15s | %15s |\n",rsmd1.getColumnName(1),rsmd1.getColumnName(2));
			System.out.println("-----------------------------------");
			while (rs1.next()) {
				System.out.format("%15s | %15s |\n",rs1.getString(1), rs1.getString("FIRST_NAME"));			
			}
			System.out.println("-----------------------------------");
			
		} catch (SQLException e) {
			
			e.printStackTrace();
		}finally {
			rs1.close();
			stm1.close();
			con1.close();	
		}

*/	  
/////////////////////////////////////////////////////  ObjectsConnectionPool Test /////////////////////////////////////////////////////////////////////////////	  

 
	
	  ObjectPool<SomeConnection> pool = new ObjectsConnectionPool();

	 
	  
	  Thread t1 = new Thread(){
		  public void run(){
			  try {
				Thread.sleep((long)Math.round(Math.random()*10000));
				SomeConnection con = pool.getInstance();
				Thread.sleep((long)Math.round(Math.random()*10000));
				pool.returnInstance(con);
				
			} catch (InterruptedException | NoMoreConnectionsException e) {
				e.printStackTrace();
			}
		  }
	  };
	  Thread t2 = new Thread(){
		  public void run(){
			  try {
				Thread.sleep((long)Math.round(Math.random()*10000));
				SomeConnection con = pool.getInstance();
				Thread.sleep((long)Math.round(Math.random()*10000));
				pool.returnInstance(con);				
				
			} catch (InterruptedException | NoMoreConnectionsException e) {
				e.printStackTrace();
			}
		  }
	  };
	  Thread t3 = new Thread(){
		  public void run(){
			  try {
				Thread.sleep((long)Math.round(Math.random()*10000));
				SomeConnection con = pool.getInstance();				
				Thread.sleep((long)Math.round(Math.random()*10000));
				pool.returnInstance(con);				
				
			} catch (InterruptedException | NoMoreConnectionsException e) {
				e.printStackTrace();
			}
		  }
	  };
	  Thread t4 = new Thread(){
		  public void run(){
			  try {
				Thread.sleep((long)Math.round(Math.random()*10000));
				SomeConnection con = pool.getInstance();			
				Thread.sleep((long)Math.round(Math.random()*10000));
				pool.returnInstance(con);		
				
			} catch (InterruptedException | NoMoreConnectionsException e) {
				e.printStackTrace();
			}
		  }
	  };	  
	  Thread t5 = new Thread(){
		  public void run(){
			  try {
				Thread.sleep((long)Math.round(Math.random()*10000));
				SomeConnection con = pool.getInstance();				
				Thread.sleep((long)Math.round(Math.random()*10000));
				pool.returnInstance(con);
				
			} catch (InterruptedException | NoMoreConnectionsException e) {
				e.printStackTrace();
			}
		  }
	  };
	  Thread t6 = new Thread(){
		  public void run(){
			  try {
				Thread.sleep((long)Math.round(Math.random()*10000));
				SomeConnection con = pool.getInstance(600);				
				Thread.sleep((long)Math.round(Math.random()*10000));
				pool.returnInstance(con);
				
			} catch (InterruptedException | NoMoreConnectionsException e) {
				e.printStackTrace();
			}
		  }
	  };  
	  Thread t7 = new Thread(){
		  public void run(){
			  try {
				Thread.sleep((long)Math.round(Math.random()*10000));
				SomeConnection con = pool.getInstance();				
				Thread.sleep((long)Math.round(Math.random()*10000));
				pool.returnInstance(con);			
				
			} catch (InterruptedException | NoMoreConnectionsException e) {
				e.printStackTrace();
			}
		  }
	  };
	  Thread t8 = new Thread(){
		  public void run(){
			  try {
				Thread.sleep((long)Math.round(Math.random()*10000));
				SomeConnection con = pool.getInstance(1000);			
				Thread.sleep((long)Math.round(Math.random()*10000));
				pool.returnInstance(con);
				
			} catch (InterruptedException | NoMoreConnectionsException e) {
				e.printStackTrace();
			}
		  }
	  };
	  Thread t9 = new Thread(){
		  public void run(){
			  try {
				Thread.sleep((long)Math.round(Math.random()*10000));
				SomeConnection con = pool.getInstance(500);				
				Thread.sleep((long)Math.round(Math.random()*10000));
				pool.returnInstance(con);				
				
			} catch (InterruptedException | NoMoreConnectionsException e) {
				e.printStackTrace();
			}
		  }
	  };
	  Thread t10 = new Thread(){
		  public void run(){
			  try {
				Thread.sleep((long)Math.round(Math.random()*10000));
				SomeConnection con = pool.getInstance();			
				Thread.sleep((long)Math.round(Math.random()*10000));
				pool.returnInstance(con);		
				
			} catch (InterruptedException | NoMoreConnectionsException e) {
				e.printStackTrace();
			}
		  }
	  };
	
	  
	  t1.start();
	  t2.start();
	  t3.start();
	  t4.start();
	  t5.start();
	  t6.start();
	  t7.start();
	  t8.start();
	  t9.start();
	  t10.start();
	
 
	  
///////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////	  


  }
}