package com.endava.java;


public class ObjectsConnectionPool extends ObjectPool<SomeConnection> {
	
	

	public ObjectsConnectionPool() {
		super();
	}

	@Override
	public SomeConnection createInstance() {
		SomeConnection c = new SomeConnection();
		return c;
	}

	@Override
	public boolean isValid(SomeConnection o) {
		
	if(o == null){
		System.out.println("NULL");
		return false;
	}
		
	
	else  
		return true;
		
		  }


	@Override
	public void closeInstance(SomeConnection o) {
		if(o != null){
			o = null;
		}else{
			throw new RuntimeException();
		}
		
	
		
	}



}
