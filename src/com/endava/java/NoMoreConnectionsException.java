package com.endava.java;

public class NoMoreConnectionsException extends Exception {

	private static final long serialVersionUID = 1997753363232807009L;
	
	public NoMoreConnectionsException(){}
	
	public NoMoreConnectionsException(String message){
		super(message);
	}
	public NoMoreConnectionsException(Throwable cause){
		super(cause);
	}
	public NoMoreConnectionsException(String message, Throwable cause){
		super(message, cause);
	}
	public NoMoreConnectionsException(String message, Throwable cause, boolean enableSuppression, boolean writableStackTrace){
		super(message,cause,enableSuppression,writableStackTrace);
	}
	
	
}
